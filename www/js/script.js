$(document).ready(function () {




    //    let count = 1;
    var discount = 0.85;


    if ($('.cart-item').hasClass('discount-item')) {
        $('.cart-item.discount-item').find('.price').addClass('akcia');
    }



    function result() {
        $('.price-mask').unmask();
        var total = 0;
        var totalDiscountPrice = 0;

        var discount = 0;
        var priceWithoutDiscount = 0;

        $('.discount-item .price-discount').each(function (i, elem) {
            $(elem).text();
            totalDiscountPrice += +$(elem).text()
            priceWithoutDiscount += +$(elem).parent().parent().find('.price').text();
        })

        $('.cart-item .price').each(function (i, elem) {
            $(elem).text();
            total += +$(elem).text()

        })

        discount = priceWithoutDiscount - totalDiscountPrice;
        $('.price-without-discount').text(total)
        $('.total-price').text(total)
        $('.total-discount').text(discount)
        $('.total-discount-price').text(total - discount);
        $('#form-result').val(total - discount)
        $('.form-result').text(total - discount)

        $('.price-mask').mask("000 000 000", {
            reverse: true
        })

    }

    result();



    function loadPage() {
        $('.price-mask').unmask();
        var total = 0;
        var totalDiscountPrice = 0;

        var discount = 0;
        var priceWithoutDiscount = 0;

        $('.cart-item .price').each(function (i, elem) {


            var count = +$(elem).parent().parent().parent().find('.count').text()

            if (count > 1) {
                $(elem).parent().siblings('.price-one').css('opacity', '1');
            }

            var item_price = +$(elem).text();
            var res = count * item_price;
            $(elem).text(res);
            total += res;
        })

        $('.discount-item .price-discount').each(function (i, elem) {
            $(elem).text();
            totalDiscountPrice += +$(elem).text()
            priceWithoutDiscount += +$(elem).parent().parent().find('.price').text();
        })

        discount = priceWithoutDiscount - totalDiscountPrice;
        $('.price-without-discount').text(total)
        $('.total-price').text(total)
        $('.total-discount').text(discount)
        $('.total-discount-price').text(total - discount);
        $('#form-result').val(total - discount)
        $('.form-result').text(total - discount)

        $('.count').each(function (i, elem) {

            if (+$(elem).text() > 1) {
                $(elem).prev().removeClass('disabled');
            }
        })



        $('.price-mask').mask("000 000 000", {
            reverse: true
        })

    }

    loadPage()





    $('.remove').click(function () {
        event.preventDefault();
        $('.price-mask').unmask();

        $(this).parent().fadeOut(300, function () {
            $(this).remove();
            result();
        });
    })

    $('.plus').click(function () {
        event.preventDefault();
        $('.price-mask').unmask();

        var count = +$(this).siblings('.count').text()

        count++

        var pr = +$(this).parent().parent().parent().find('.price-one-number').text()

        $(this).parent().parent().parent().find('.price').text(count * pr);

        if (count > 1 && $(this).parent().parent().parent().hasClass('discount-item')) {
            var priceDiscount = +$(this).parent().parent().parent().find('.price-discount').text(count * pr * discount);
            var priceDiscountText = +$(this).parent().parent().parent().find('.price-discount').text();
            var round = priceDiscountText.toFixed();

            $(this).parent().parent().parent().find('.price-discount').text(round);
        }

        if (count > 1) {
            $(this).parent().parent().parent().find('.price-one').css('opacity', '1');
        }

        $(this).siblings('.count').text(count);
        $(this).siblings('.minus').removeClass('disabled');

        result()


    })




    $('.minus').click(function () {
        event.preventDefault();
        $('.price-mask').unmask();

        var count = +$(this).siblings('.count').text()

        count--

        var pr = +$(this).parent().parent().parent().find('.hidden-price').text()

        if (count == 1) {
            $(this).addClass('disabled');
            $(this).parent().parent().parent().find('.price').text(1 * pr)
            $(this).parent().parent().parent().find('.price-discount').text(1 * pr);
            $(this).parent().parent().parent().find('.price-one').css('opacity', '0');

        } else if (count >= 0 && !$(this).parent().parent().parent().hasClass('discount-item')) {
            $(this).parent().parent().parent().find('.price').text(count * pr);

        } else if (count >= 0 && $(this).parent().parent().parent().hasClass('discount-item')) {
            $(this).parent().parent().parent().find('.price').text(count * pr);

            var priceDiscount = +$(this).parent().parent().parent().find('.price-discount').text(count * pr * discount);
            var priceDiscountText = +$(this).parent().parent().parent().find('.price-discount').text();
            var round = priceDiscountText.toFixed();
            $(this).parent().parent().parent().find('.price-discount').text(round);
        }

        $(this).siblings('.minus').removeClass('disabled');
        $(this).siblings('.count').text(count);


        result()


    })
});
